package com.silviuned.utils;

public class Configs {
	public static final String INPUT_FILE = "Problema01";
	//public static final int NR_MAX_GENERATII = 200000;
	public static final int NR_MAX_GENERATII = 100000;
	public static final int NR_CROMOZOMI = 100;
	public static final double RATA_INCRUCISARE = 0.05;
	public static final double RATA_MUTATIE = 0.4;
	//public static final double RATA_INCRUCISARE = 0.002;
	//public static final double RATA_MUTATIE = 0.02;
	//public static final double RATA_INCRUCISARE = 0.65;
	//public static final double RATA_MUTATIE = 0.15;
	
	// 4 -> 3 -> 6
	// 5 -> 4 -> 4
	
	// Generarea datelor:	Mereu pe rand sunt cate o cifra din fiecare.
	// Incrusisarea: 		Incrucisez randurile, astfel randurile mereu raman valide.
	// Mutatie:				Swap (50%), 3-swap (30%), insertion (20%).
	// Modific operator
	// Statistica, aceeasi instanta x 5 instante, media, stdev, concluzie, box plotturi.
	// In 2-3 saptamani.
}
