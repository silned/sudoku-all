package com.silviuned.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

	public static List<List<Integer>> getMissingGenes(int[][] input) {
		
		List<List<Integer>> missingGenes = new ArrayList<>();
		for (int i = 0; i < input.length; i++) {
			List<Integer> missingGenesRowI = new ArrayList<>();
			for (int j = 0; j < input.length; j++) {
				missingGenesRowI.add(j + 1);
			}

			// Eliminam genele deja existente
			for (int j = 0; j < input.length; j++)
			{
				if (input[i][j] != 0) {
					missingGenesRowI.remove(new Integer(input[i][j]));
				}
			}
			
			missingGenes.add(missingGenesRowI);
		}

		return missingGenes;
	}
}
