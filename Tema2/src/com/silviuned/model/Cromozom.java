package com.silviuned.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Cromozom {

	private int[][] genes;
	
	public Cromozom(int[][] input, List<List<Integer>> missingGenes) {
		genes = new int[input.length][input.length];
		for (int i = 0; i < input.length; i++) {
			List<Integer> missingGenesRowI = missingGenes.get(i);
			Collections.shuffle(missingGenesRowI);
			
			int missingGenesIterator = 0;
			for (int j = 0; j < input.length; j++) {
				if (input[i][j] != 0) {
					genes[i][j] = input[i][j];
				} else {
					genes[i][j] = missingGenesRowI.get(missingGenesIterator);
					missingGenesIterator++;
				}
			}
		}
	}
	
	public Cromozom(int[][] genes) {
		this.genes = new int[genes.length][genes.length];
		for (int i = 0; i < genes.length; i++) {
			for (int j = 0; j < genes.length; j++) {
				this.genes[i][j] = genes[i][j];
			}
		}
	}
	
	public int getFitness() {
		int totalError = 0;
		HashSet<Integer> numberPool = new HashSet<>();
		
		/* Calcularea erorilor pe randuri */
		for (int i = 0; i < genes.length; i++) {
			numberPool.clear();
			
			for (int j = 0; j < genes.length; j++) {
				numberPool.add(genes[i][j]);
			}
			
			totalError += genes.length - numberPool.size();
		}
		
		/* Calcularea erorilor pe coloane */
		for (int j = 0; j < genes.length; j++) {
			numberPool.clear();
			
			for (int i = 0; i < genes.length; i++) {
				numberPool.add(genes[i][j]);
			}
			
			totalError += genes.length - numberPool.size();
		}
		
		/* Calcularea erorilor in sub-patrate */
		int k = (int) Math.sqrt(genes.length);
		for (int q = 0; q < k; q++) {
			for (int p = 0; p < k; p++) {
				numberPool.clear();
				
				for (int i = 0; i < k; i++) {
					for (int j = 0; j < k; j++) {
						numberPool.add(genes[q * k + i][p * k + j]);
					}
				}
				
				totalError += genes.length - numberPool.size();
			}
		}
		
		int maxErrors = 3 * genes.length * genes.length;
		return maxErrors - totalError;
	}
	
	public void incrucisare(Cromozom other) {
		for (int i = 0; i < genes.length; i++) {
			if (Math.random() > 0.5) {
				int[] row = other.getGenes()[i].clone();
				other.getGenes()[i] = genes[i].clone();
				genes[i] = row;
			}
		}
	}
	
	public void mutatie(int rowNr, int posOne, int posTwo) {
		int k = genes[rowNr][posOne];
		genes[rowNr][posOne] = genes[rowNr][posTwo];
		genes[rowNr][posTwo] = k;
	}
	
	public void mutatie(int rowNr, int posOne, int posTwo, int posThree) {
		int k = genes[rowNr][posOne];
		genes[rowNr][posOne] = genes[rowNr][posTwo];
		genes[rowNr][posTwo] = genes[rowNr][posThree];
		genes[rowNr][posThree] = k;
	}
	
	public int[][] getGenes() {
		return this.genes;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < genes.length; i++) {
			for (int j = 0; j < genes.length; j++) {
				sb.append(genes[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	
}
