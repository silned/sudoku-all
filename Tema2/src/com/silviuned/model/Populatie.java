package com.silviuned.model;

import static com.silviuned.utils.Configs.*;
import static com.silviuned.utils.Utils.getMissingGenes;

import java.util.List;

import com.silviuned.service.DataFetcher;


public class Populatie {

	private int bestFitness;
	private Cromozom bestSolution;
	
	private int[] fitness;
	private Cromozom[] cromozomi;
	
	private int[][] input;
	
	public Populatie() {
		input = DataFetcher.getInput();
		cromozomi = new Cromozom[NR_CROMOZOMI];
		fitness = new int[NR_CROMOZOMI];
		
		List<List<Integer>> missingGenes = getMissingGenes(input);
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			cromozomi[i] = new Cromozom(input, missingGenes);
		}
	}
	
	public void evaluare() {
		bestFitness = 0;
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			fitness[i] = cromozomi[i].getFitness();
			if (fitness[i] > bestFitness) {
				bestFitness = fitness[i];
				bestSolution = cromozomi[i];
			}
		}
	}
	
	public void turneu(){
		Cromozom[] populatieNoua = new Cromozom[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++){
			int randOne = (int)(Math.random() * NR_CROMOZOMI);
			int randTwo = (int)(Math.random() * NR_CROMOZOMI);
			
			if (randOne == NR_CROMOZOMI) {
				randOne--;
			}
			if (randTwo == NR_CROMOZOMI) {
				randTwo--;
			}
			
			if (fitness[randOne] >= fitness[randTwo]){
				populatieNoua[i] = new Cromozom(cromozomi[randOne].getGenes());
			} else {
				populatieNoua[i] = new Cromozom(cromozomi[randTwo].getGenes());	
			}
		}
		
		this.cromozomi = populatieNoua;
	}
	
	public void roataNorocului() {
		int fitnessTotal = 0;
		for (int f : fitness) {
			fitnessTotal += f;
		}
		
		double sumaFitness = 0;
		double[] fitnessCumulat = new double[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++){
			sumaFitness = sumaFitness + (fitness[i] * 1.0 / fitnessTotal);
			fitnessCumulat[i] = sumaFitness;
		}
		
		/* Creare generatie noua. */
		Cromozom[] populatieNoua = new Cromozom[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			double rand = Math.random();
			
			int j = 0;
			while (rand > fitnessCumulat[j]) {
				j++;
			}
			
			if (j >= NR_CROMOZOMI) {
				j = NR_CROMOZOMI - 1;
			}
			
			populatieNoua[i] = new Cromozom(cromozomi[j].getGenes());
		}
		
		this.cromozomi = populatieNoua;
	}
	
	public void incrucisare() {
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			if (Math.random() < RATA_INCRUCISARE) {
				int j = (int) Math.random() * NR_CROMOZOMI;
				if (j == NR_CROMOZOMI) {
					j--;
				}
				
				if (i != j) {
					cromozomi[i].incrucisare(cromozomi[j]);
				}
			}
		}
	}
	
	public void mutatie() {
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			if (Math.random() < RATA_MUTATIE) {
				for (int j = 0; j < input.length; j++) {
					double random = Math.random();
					if (random > 0.5) {
						//Swap
						int posOne = getMutationPositon(j, -1);
						int posTwo = getMutationPositon(j, -1);
						
						cromozomi[i].mutatie(j, posOne, posTwo);
					} else if (random > 0.2) {
						//3-swap
						int posOne = getMutationPositon(j, -1);
						int posTwo = getMutationPositon(j, -1);
						int posThree = getMutationPositon(j, -1);
						
						cromozomi[i].mutatie(j, posOne, posTwo, posThree);
					} else {
						// Insertion
						int posOne = getMutationPositon(j, -1);
						int posTwo = getMutationPositon(j, -1);
						
						mutatiePrinInsertie(cromozomi[i].getGenes(), j,  posOne,  posTwo);
					}
				}
				
				
				int rowNr = (int) (Math.random() * (input.length - 1)) + 1;
				if (rowNr == input.length) {
					rowNr--;
				}
				
				int posOne = getMutationPositon(rowNr, -1);
				int posTwo = getMutationPositon(rowNr, -1);
				
				cromozomi[i].mutatie(rowNr, posOne, posTwo);
			}
		}
	}
	
	public int getMutationPositon(int rowNr, int besidePos) {
		int pos = -1;
		
		while ((pos == -1) || input[rowNr][pos] != 0 || pos == besidePos) {
			pos = (int) (Math.random() * input.length);
			if (pos == input.length) {
				pos--;
			}
		}
		
		return pos;
	}
	
	public void mutatiePrinInsertie(int[][] genes, int rowNr, int posOne, int posTwo) {
		if (posOne != posTwo) {
			// Punem in posOne valoarea mai mica.
			if (posOne > posTwo) {
				int k = posOne;
				posOne = posTwo;
				posTwo = k;
			}
			
			int value = genes[rowNr][posOne];
			genes[rowNr][posOne] = genes[rowNr][posTwo];
			for (int i = posOne + 1; i <= posTwo; i++) {
				if (input[rowNr][i] != 0) {
					continue;
				} else {
					int k = value;
					value = genes[rowNr][i];
					genes[rowNr][i] = k;
				}
			}
		}
	}
	
	public int getBestErrorCount() {
		int maxErrors = 3 * bestSolution.getGenes().length * bestSolution.getGenes().length;
		return maxErrors - bestFitness;
	}
	
	public Cromozom getBestSolution() {
		return bestSolution;
	}
}
